\input{include/header.tex}
\input{include/theories/bool.tex}
\input{include/theories/ht.tex}
\input{include/theories/verilog.tex}

% auto-generated glossary terms
\input{notes/2.glt}

\indextitle{Programming with Boolean logic}
\subtitle{Words and truthiness}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage



\section{Introduction}

  You're certainly already aware that every modern programming languages has a \vocab{data-type} of ``Boolean values''. How exactly do these data types relate to \vocab{Boolean-logic}?

  First, we should note that the \texttt{true} data value in C++ (for example) is \insist{not} exactly the same thing as the $\true$ symbol that we've been writing on paper. In particular, \texttt{true} in C++ is the size of one \vocab{machine-word}, not just a single bit.

  To be very precise, we might say that a C++ program \insist{simulates} or \insist{implements} Boolean logic using machine words instead of Boolean truth values. In these notes, we will explore how this simulation works, and how it interacts with \vocabs{conditional} like \texttt{if}/\texttt{then} statements. We will also explore how some higher-level languages like Python extend this low-level simulation with the concept of \vocab{truthy} and \vocab{falsy} values.


\section{Bits and words}

  Most of this section will be review from previous courses, but we should be precise about our world of bits as we connect it to our world of logic.

  \subsection{Terminology}

    A \vocab{bit} is a single digit from the set \{0, 1\}. A \vocab{bit-vector} (or ``bit array'') is a sequence of bits, like 101 or 00000. A \vocab{byte} is a bit vector of length 8.

    Each kind of \vocab{memory} (RAM, cache, disk, etc.) on a single computer can be thought of as a single bit vector. For example, most modern systems have at least a couple gigabytes of RAM; four gigabytes of RAM is a bit vector of length $4*8*2^{30}$ (4 gigabytes, 8 bits per byte, $2^{30}$ bytes per gigabyte).

    (To be very technical, $2^{30}$ bytes is a \href{https://simple.wikipedia.org/wiki/Gibibyte}{\linkstyle{gibibyte}}, not a \href{https://simple.wikipedia.org/wiki/Gigabyte}{\linkstyle{gigabyte}}, but it is usually what you get when you buy ``a gigabyte of RAM''. Either way, the point is that it is just a large bit vector.)

    The term \vocab{machine-word} (or just ``word'') refers to any bit vector of a specific length, called the \vocab{word-size}. The word size is determined by the processor architecture, so it is constant on a single processor, but it can vary between different processors.

    Most modern processors have a word size of 64 bits, which means that a machine word is a bit vector of length 64. It is important to note that this is not a universally valid assumption, though!

  \subsection{Data types}

    In most programming languages, the word size of the computer determines the size of several fundamental data types, including the built-in Boolean type. This means \texttt{true} and \texttt{false} in C++ each represent bit vectors of length 64 on most modern computers.

    The keyword \texttt{false} represents a bit vector of all 0s. The keyword \texttt{true} is actually a little more complex to describe! It is \insist{not} guaranteed to be all 0s ending in a single 1.

  \subsection{Conditionals}

    Instead of asking about \texttt{true}'s precise bit pattern, we should ask how it \insist{interacts} with \vocabs{conditional} like \texttt{if}/\texttt{then} statements.

    When a C++ program executes the statement \texttt{if (b) then \{ \ldots\ \} else \{ \ldots\ \}}, it will execute the \texttt{then} case \insist{unless} \texttt{b} is same bit vector as \texttt{false}. This means that \insist{any} bit vector except \texttt{false} will ``count'' as a \texttt{true} value in an \texttt{if}/\texttt{else} statement.

    In fact, C++ \insist{only} guarantees that when you write \texttt{true}, it will be represented by a non-\texttt{false} bit vector. It might not even be the same bit vector every time!

\section{Operations}

  \subsection{Bitwise}

    A \vocab{bitwise} operation is a logical operation applied to \insist{each bit} of its inputs to produce each bit of its outputs. Bitwise operators are related to logical operators, but are \insist{not the same}!

    C++ and many other languages use this notation for bitwise and logical operators:

    \begin{center}
      \begin{tabular}{r | c | c | c | c}
        name & abbreviation & logic on paper & C++ bitwise & C++ logical \\
        \thickhline
        negation & NOT & $\neg$ & \texttt{\textasciitilde} & \texttt{!} \\
        \hline
        conjunction & AND & $\conj$ & \texttt{\&} & \texttt{\&\&} \\
        \hline
        disjunction & OR & $\disj$ & \texttt{|} & \texttt{||} \\
        \hline
        exclusive disjunction & XOR & $\xdisj$ & \texttt{\^} & (none)
      \end{tabular}
    \end{center}

    Bitwise negation is \insist{not} the same as logical negation! For example, the bit vector 101 is a \texttt{true} value, but its bitwise negation 010 is \insist{also} a \texttt{true} value.

    Similarly, bitwise conjunction is \insist{not} the same as logical conjunction. For example, \texttt{11000 \& 00011 = 00000} is taking two \texttt{true} values as input and producing a \texttt{false} value as output.

  \subsection{Arithmetic}

    How do the logical operators in C++ correspond to our logical operators on paper? In particular, if bitwise operations are not the same as logical operations, how \insist{do} we implement logical operations over bit vectors?

    There are many different equivalent ways to simulate Boolean logic by calculating with bit vectors. One interesting way involves interpreting machine words as numbers, making use of common patterns that are shared between logical operations and numeric operations.

    Recall that a bit vector can be interpreted as a number in base 2. For this discussion, we will think of a bit vector as an \vocab{unsigned} (non-negative) base 2 number.

    Under this numeric interpretation of bit vectors, the number 0 is the \texttt{false} bit vector, and any non-zero number is a \texttt{true} bit vector. We will describe these two cases with the symbols \texttt{Z} for ``zero'' and \texttt{NZ} for ``non-zero''.

    Consider this lookup table defining the \texttt{min} operation over the set \{\texttt{Z}, \texttt{NZ}\}.

    \begin{center}
      \begin{tabular}{c c ? c | c}
        \multicolumn{2}{c}{} & \multicolumn{2}{c}{\texttt{y}} \\
        & \texttt{min(x, y)} & \texttt{Z} & \texttt{NZ} \\
        \thickcline{2-4}
        \multirow{2}{*}{\texttt{x}}
        & \texttt{Z} & \texttt{Z} & \texttt{Z} \\
        \cline{2-4}
        & \texttt{NZ} & \texttt{Z} & \texttt{NZ}
      \end{tabular}
    \end{center}

    This should make sense based on your existing understanding of arithmetic: the minimum of two numbers can only be non-zero if both input numbers are non-zero. Note how the table for the \texttt{min} operation over the set \{\texttt{Z}, \texttt{NZ}\} has the \insist{exact} same pattern as our standard Boolean definition for conjunction:

    \begin{center}
      \begin{tabular}{c c ? c | c}
        \multicolumn{2}{c}{} & \multicolumn{2}{c}{$Q$} \\
        & $P \conj Q$ & \false & \true \\
        \thickcline{2-4}
        \multirow{2}{*}{$P$}
        & \false & \false & \false \\
        \cline{2-4}
        & \true & \false & \true
      \end{tabular}
    \end{center}

    This gives us a simple algorithm for calculating the logical conjunction of two bit vectors: treat them as unsigned integers and return the minimum.

    This is also consistent with our general understanding of conjunction as an operator that outputs the ``least true'' of its inputs: when we treat truth values as unsigned integers, this ``less true'' relation becomes our literal < operator.

    In fact, this interpretation gives us convenient implementations for each of our most important logical operators:

    \begin{center}
      \begin{tabular}{c | c}
        logic & unsigned arithmetic \\
        \thickhline
        $\conj$ & \texttt{min} \\
        \hline
        $\disj$ & \texttt{max} \\
        \hline
        $\impl$ & $\le$
      \end{tabular}
    \end{center}

    The odd one out is logical negation, which does not have a very direct implementation in terms of a traditional artihmetic operator. Instead, we can just define that \texttt{!x} outputs 1 if \texttt{x} = 0, or outputs 0 otherwise.


\section{Short-circuiting}

  You may be familiar with the feature of \vocab{short-circuiting} operators in programming languages. For example, consider this short C++ program:

\begin{verbatim}  bool f() {
    cout << 1;
    return true;
  }

  bool g() {
    cout << 2;
    return true;
  }

  void shortCircuiting() {
    if (f() || g()) {
      cout << 3;
    }
  }

  void nonShortCircuiting() {
    if (f() && g()) {
      cout << 3;
    }
  }\end{verbatim}

  With these function definitions, we will get these printed outputs:

  \begin{center}
    \begin{tabular}{r | l}
      \multicolumn{1}{c |}{function call} & \multicolumn{1}{c}{printed output} \\
      \thickhline
      \texttt{shortCircuiting()} & \texttt{13} \\
      \hline
      \texttt{nonShortCircuiting()} & \texttt{123}
    \end{tabular}
  \end{center}

  In both calls, the \texttt{if} condition is true, so \texttt{3} is printed. The difference is that the call to \texttt{shortCircuiting()} \insist{does not call \texttt{g()}}, so \texttt{2} is not printed. This is the \insist{expected} behavior of these functions in any modern language.

  This behavior is logically valid because of a Boolean principle that we've seen before: \vocab{disjunction} can be implemented with an \texttt{if}/\texttt{else} statement, as in this pseudocode.

  \begin{verbatim}  or(p, q):
    if (p):
      return true
    else:
      return q\end{verbatim}

  When \texttt{p} is \texttt{true}, we do not need to \insist{know} the value of \texttt{q} in order to \insist{know} the value of \texttt{p || q}, so C++ does not need to \insist{compute} the value of \texttt{q} in order to \insist{compute} the value of \texttt{p || q}. This holds even when \texttt{q} is an expression that might involve operators or function calls.




\section{Truthy and falsy values}

  Many modern programming languages also allow types \insist{other} than Boolean data values to be used as the condition in an \texttt{if}/\texttt{else} statement. This technique is most notable in dynamically-typed languages like Python, JavaScript, and Bash, but it is also present in many statically-typed languages.

  Earlier in these notes, we discussed how an \texttt{if}/\texttt{else} statement in C++ checks its condition: specifically, \texttt{if (b) s$_1$ else s$_2$} will execute \texttt{s$_2$} if \texttt{b} is a bit vector containing only 0s, and will execute \texttt{s$_1$} otherwise.

  In fact, C++ also allows us to use types other than \texttt{bool} as conditions in \texttt{if}/\texttt{else} statements, under the same principle: a value with all bits set to 0 is ``false'', and \insist{any other bit vector} is ``true''.

  For example, we can use an \texttt{if}/\texttt{else} statement in C++ to check whether a pointer is \texttt{NULL}:

  \begin{verbatim}  void checkNull(int *x) {
    if (x)
      cout << "the argument is not NULL";
    else
      cout << "the argument is NULL";
  }\end{verbatim}

  This works because \texttt{NULL} is guaranteed to be a bit vector with all bits set to 0, and all non-\texttt{NULL} pointers are guaranteed to have at least one bit set to 1.

  \subsection{Terminology}

    We know that \texttt{NULL} \insist{behaves like} \texttt{false} in an \texttt{if}/\texttt{else} statement, and any non-\texttt{NULL} pointer \insist{behaves like} \texttt{true} in an \texttt{if}/\texttt{else} statement. Still, it seems inaccurate to say that a pointer value is ``true'' or ``false'': a pointer is not a logical truth value, it's an address in a memory space.

    In general, we often say that a value is \vocab{falsy} if it \insist{behaves like} \texttt{false} in an \texttt{if}/\texttt{else} statement, and \vocab{truthy} if it \insist{behaves like} \texttt{true} in an \texttt{if}/\texttt{else} statement. We might say that \texttt{NULL} is a falsy value in C++.

  \subsection{Patterns}

    The behavior of truthy and falsy values extends to conjunction and disjunction: for example, if \texttt{x} is a non-null pointer in C++, \texttt{NULL \&\& x = NULL} and \texttt{NULL || x = x}.

    Together with short-circuiting, this gives us a concise pattern for checking properties on values which might be \texttt{NULL} in C++ (or some other kind of falsy value in a different language):

    \begin{verbatim}  if (x && x->y && x->y->z) { ... }\end{verbatim}

    The \texttt{||} operator can also be used to provide a ``default'' for another value that might be \texttt{NULL}:

    \begin{verbatim}  int* x = y || z;\end{verbatim}

    These patterns work in C++ because of the way that it treats all values as bit vectors. In higher-level languages like C\# and Swift, not all values are treated as bit vectors, and the \texttt{null} value is not always guaranteed to be represented by the same bit vector as the \texttt{false} value.

    In these settings, there is sometimes a \vocab{null-coalescing} operator written \texttt{??}, which behaves like \texttt{||} except that it returns the ``most non-null'' operand instead of the ``most true'' operand. In C++, these happen to be the same thing because of how \texttt{NULL} and \texttt{false} are defined.


\input{include/footer.tex}
\end{document}
