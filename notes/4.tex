\input{include/header.tex}
\input{include/theories/bool.tex}

% auto-generated glossary terms
\input{notes/4.glt}

\indextitle{Boolean algebra}
\subtitle{Value-preserving transformations and the structure of logic}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage



\section{Introduction}

  In the previous notes, we discussed the ``structure'' of \vocab{Boolean-logic} \vocab{expressions}: an expression can be represented as an \vocabstyle{\acrlong{ast}} (AST), and we might say that two expressions have ``the same structure'' if they share the same AST.

  What about the ``structure'' of Boolean logic \insist{itself}? When we talk about the ``structure'' of an entire system of logic, we are focusing on its ``defining features'': what makes Boolean logic \insist{different} from other non-Boolean logics?

  This question is closely related to our concept of a ``simulation'' or ``implementation'' of Boolean logic, which we discussed in our notes on ``programming with Boolean logic''. We might ask: what exactly is \insist{required} when we ``implement'' Boolean logic? In other words, what makes something a \insist{valid} or \insist{invalid} ``implementation'' of Boolean logic?

  We have already explored some aspects of these questions, but we have not yet seen a complete answer to them. In these notes, we will study these questions from the perspective of \vocab{Boolean-algebra}, along with a survey of the algebraic perspective on logic in general.


\section{Equality}

  In order to reason algebraically, we first need to be precise about our concept of \vocab{equality}. The precise definition of \vocab{equality} varies between domains, and can often be surprisingly subtle.

  In the setting of Boolean logic, we often define our ``standard'' form of \vocab{equality} to be \vocablink{tautology}{tautological} \vocab{equivalence}. We've discussed this definition a bit in lecture already, but here we will make it more precise:

  \begin{center}
    \begin{tabular}{c | l}
      when we write & what we mean is \\
      \thickhline
      $e_1\ \equals\ e_2$ & claim: $e_1 \equ e_2$ is a tautology \\
      \hline
      $e_1\ \neq\ e_2$ & claim: $e_1 \equ e_2$ is not a tautology \\
      \hline
      $e_1\ \mightequals\ e_2$ & question: is $e_1 \equ e_2$ a tautology or not?
    \end{tabular}
  \end{center}

  Note carefully that $e_1 \neq e_2$ does not claim that $e_1 \equ e_2$ is a \vocab{contradiction}, just that it is \insist{not} a \vocab{tautology}. For example, we have $P \impl Q \neq Q \impl P$ because the two sides are \insist{not always} equivalent, even though they are \insist{sometimes} equivalent (specifically when $P$ and $Q$ have the same truth value).

  What is the difference between the \vocab{sym-equals} operator and the \vocab{sym-equ} operator? We might say that the $\equ$ operator is ``inside'' our system \insist{of} Boolean logic, while the $=$ operator is ``outside'' Boolean logic: the $=$ operator is part of our reasoning \insist{about} Boolean logic.

  When we write $e_1 \equ e_2$, we are just writing a Boolean expression. We are not necessarily making any claim \insist{about} the expression: it might be a tautology, or a contradiction, or neither. For example, it is not ``incorrect'' in general to write $P \equ \neg P$: it happens to be a contradiction, but it is still a meaningful Boolean expression.

  When we write $e_1 = e_2$, we are making a claim \insist{about} the Boolean expression $e_1 \equ e_2$. It \insist{is} generally incorrect to write $P = \neg P$, because it is incorrect to \insist{claim that $P \equ \neg P$ is a tautology}. (By the same reasoning, it is correct to write $P \neq \neg P$.)


\section{Value-preserving transformations}

  In our last set of notes, we covered a precise definition of how algebraic rules like $x + y = y + x$ work: a rule is valid to use exacly when it \vocabs{subsume} the particular equation that we are trying to use it with.

  \subsection{Transforming an entire expression}

    More generally, we can read a rule like $x + y = y + x$ as a way to ``transform'' one expression into another:

    \begin{enumerate}
      \item
        Identify the expression that we want to transform: for example, let's use $1 + 2$.
      \item
        Set up a \vocab{unification-problem} with the \insist{left side} of the rule: in this example, we have $1 + 2\ \vocab{sym-subsumedby}\ x + y$.
      \item
        Solve the unification problem to obtain a \vocab{unifying-substitution}: in this example, the solution is $\sigma\ \vocab{sym-defequals}\ \left[x \defequals 1,\ y \defequals 2\right]$. (If there is no possible unifying substitution, that means the rule is not valid for the expression we are trying to use it with.)
      \item
        \Vocab{apply} the unifying substitution to the \insist{right side} of the rule: in this example, we have $\left[x \defequals 1,\ y \defequals 2\right](y + x)\ \vocab{sym-identical}\ 2 + 1$.
      \item
        Conclude that our original expression is equal to our transformed expression: in this example, $1 + 2 = 2 + 1$. (Note that the two expressions are \insist{not} \vocab{identical}, just \vocab{equal}.)
    \end{enumerate}

    This process is exactly the same for Boolean algebra as for numeric algebra: it is a fundamental technique of algebra in general.

    Why is step 5 a valid conclusion? The principle is that an algebraic rule is a \vocab{value-preserving} transformation: we have a guarantee that the \insist{value} of the expression will stay the same even as its \insist{structure} changes. (This is guaranteed by our definition of \vocab{equality}, which is part of why we need to be very precise about that definition when we're doing algebra.)

    In the setting of Boolean logic, where our values are Boolean truth values, we might use the phrase ``\vocab{truth-preserving} transformation'' to describe the relevant special case of a \vocab{value-preserving} transformation. The guarantee is that the \vocab{truth-value} of the expression will stay the same even as its \vocabstyle{\acrshort{ast}} changes.

  \subsection{Transforming a subexpression}

    Critically, we are able to use this technique with any \insist{subexpression} of a larger expression, with the same guarantee that the transformation is \vocab{value-preserving}.

    Our process is only slightly modified:

    \begin{enumerate}
      \item
        Identify the expression that we want to transform: for example, let's use $-(1 + 2) \times 3$. It is helpful to visualize the expression as an AST:

        \begin{center}
          \begin{tabular}{c}
            overall expression \\
            \thickhline
            \begin{forest}
              [$\times$
                [$-$
                  [$+$
                    [1]
                    [2]
                  ]
                ]
                [3]
              ]
            \end{forest}
          \end{tabular}
        \end{center}

      \item
        Identify the \insist{subexpression} that we want to transform: for example, let's focus on $1 + 2$. It is helpful to visualize this as an action of \insist{separating} the subexpression AST from the rest of the AST, leaving a ``hole'' behind (represented here with the $\square$ symbol):

        \begin{center}
          \begin{tabular}{c | c}
            overall expression & subexpression \\
            \thickhline
            \begin{forest} baseline
              [$\times$
                [$-$
                  [$\square$]
                ]
                [3]
              ]
            \end{forest}
            &
            \begin{forest} baseline
              [$+$
                [1]
                [2]
              ]
            \end{forest}
          \end{tabular}
        \end{center}

      \item
        Transform the subexpression using the rule, in this example $x + y = y + x$:

        \begin{center}
          \begin{tabular}{c | c}
            overall expression & subexpression \\
            \thickhline
            \begin{forest} baseline
              [$\times$
                [$-$
                  [$\square$]
                ]
                [3]
              ]
            \end{forest}
            &
            \begin{forest} baseline
              [$+$
                [2]
                [1]
              ]
            \end{forest}
          \end{tabular}
        \end{center}

      \item
        Finally, ``plug'' the transformed subexpression back into the ``hole'':

        \begin{center}
          \begin{tabular}{c}
            overall expression \\
            \thickhline
            \begin{forest}
              [$\times$
                [$-$
                  [$+$
                    [2]
                    [1]
                  ]
                ]
                [3]
              ]
            \end{forest}
          \end{tabular}
        \end{center}

      \item
        Conclude that our original \insist{overall} expression is equal to our transformed \insist{overall} expression: in this example, $-(1 + 2) \times 3 = -(2 + 1) \times 3$.
    \end{enumerate}

    \insist{As long as we are consistent about how we ``separate'' the subexpression and then ``plug'' it back in}, this process of transforming subexpressions is still \vocab{value-preserving}.


\section{Transitive reasoning}

  Another fundamental algebraic technique is the ability to ``chain'' multiple rules together into a step-by-step proof. In formal terms, we often call this technique \vocab{transitive-reasoning}.

  \subsection{With equalities}

    For example, we might show that $a \times (b + c) = (c \times a) + (b \times a)$ with a ``chain'' of equalities:

    \begin{center}
      \begin{tabular}{r c l}
        $a \times (b + c)$ & = & $a \times (c + b)$ \\
        & = & $(c + b) \times a$ \\
        & = & $(c \times a) + (b \times a)$
      \end{tabular}
    \end{center}

    This is valid precisely because \vocab{equality} of arithmetic expressions is \vocab{transitive}: if $e_1 = e_2$ and $e_2 = e_3$, then $e_1 = e_3$. This is a property we \insist{almost} always expect of any operator that we might use the term \vocab{equality} for.

    (The main exception is that most forms of ``approximate equality'' are not transitive. In particular, equality of floating-point numbers is not transitive, because floating-point arithmetic is always approximate. JavaScript's \texttt{==} operator is also not transitive in general, but that's just because JavaScript is badly designed; good JavaScript programmers always use the \texttt{===} operator instead, which behaves the same way that every other language's \texttt{==} operator behaves.)

    It's very common for us to do transitive reasoning with equalities, so we use the term \vocab{equational-reasoning} to describe this particular application of the general technique of transitive reasoning.

    Transitivity is one of three properties that we usually expect an ``equality'' operator to have. Here are all three of these properties listed together:

    \begin{center}
      \begin{tabular}{c | c | c}
        noun & meaning & tautology \\
        \thickhline
        reflexivity & ``everything is equal to itself'' & $P \equ P$ \\
        \hline
        symmetry & ``equalities can be reversed'' & $(P \equ Q) \impl (Q \equ P)$ \\
        \hline
        transitivity & ``equalities can be chained'' & $(P \equ Q) \conj (Q \equ R) \impl (P \equ R)$
      \end{tabular}
    \end{center}

  \subsection{With inequalities}

    In general, it is actually valid to do transitive reasoning with \insist{any} operator that is transitive. For example, we can do transitive reasoning with the $\le$ operator in standard arithmetic:

    \begin{center}
      \begin{tabular}{r c l}
        $a \times (b + c)$ & $\le$ & $a \times (c + b)$ \\
        & $\le$ & $(c + b) \times a$ \\
        & $\le$ & $(c + b) \times a + 1$ \\
        & $\le$ & $((c \times a) + (b \times a)) + 1$
      \end{tabular}
    \end{center}

    The third step here (going from $(z + y) \times x$ to $(z + y) \times x + 1$ would \insist{not} be valid if we were reasoning about \vocab{equality}, but it \insist{is} valid when we reason about the $\le$ operator, because we have the rule $x \le x + 1$.

    In Boolean logic, \vocab{implication} is transitive: if $P \impl Q$ and $Q \impl R$, then $P \impl R$. (Note that we do not need the reverse to be true: there are some cases where $P \impl R$ is true but $P \impl Q$ or $Q \impl R$ might be false, but this is not a problem for our transitive reasoning.)

    We can demonstrate the transitivity of implication with a truth table:

    \begin{center}
      \begin{tabular}{c | c | c ? c ? c ? c ? c ? c}
        $P$ & $Q$ & $R$ & $P \impl Q$ & $Q \impl R$ & $(P \impl Q) \conj (Q \impl R)$ & $P \impl R$ & $(P \impl Q) \conj (Q \impl R) \impl (P \impl R)$\\
        \thickhline
        \true & \true & \true & \true & \true & \true & \true & \true \\
        \hline
        \true & \true & \false & \true & \false & \false & \false & \true \\
        \hline
        \true & \false & \true & \false & \true & \false & \true & \true \\
        \hline
        \true & \false & \false & \false & \true & \false & \false & \true \\
        \hline
        \false & \true & \true & \true & \true & \true & \true & \true \\
        \hline
        \false & \true & \false & \true & \false & \false & \true & \true \\
        \hline
        \false & \false & \true & \true & \true & \true & \true & \true \\
        \hline
        \false & \false & \false & \true & \true & \true & \true & \true
      \end{tabular}
    \end{center}

    In other words, we might say that $P \impl R$ is always ``at least as true'' as both $(P \impl Q)$ and $(Q \impl R)$.

    This means we can do transitive reasoning with any \vocablink{tautology}{tautological} \vocab{implication}, not just \vocab{equality} (\vocablink{tautology}{tautological} \vocab{equivalence}). We will use the symbol $\le$ to represent a tautological implication: the telationship between $\impl$ and $\le$ is the same as the relationship between $\equ$ and $=$.

    \begin{center}
      \begin{tabular}{c | l}
        when we write & what we mean is \\
        \thickhline
        $e_1\ \le\ e_2$ & claim: $e_1 \impl e_2$ is a tautology \\
        \hline
        $e_1\ \nleq\ e_2$ & claim: $e_1 \impl e_2$ is not a tautology \\
        \hline
        $e_1\ \mightle\ e_2$ & question: is $e_1 \impl e_2$ a tautology or not?
      \end{tabular}
    \end{center}


    For example, using the rule $P \le (P \disj Q)$ three times:

    \begin{center}
      \begin{tabular}{r c l}
        $A$ & $\le$ & $A \disj B$ \\
        & $\le$ & $A \disj (B \disj C)$ \\
        & $\le$ & $(A \disj C) \disj (B \disj C)$
      \end{tabular}
    \end{center}

    Note that this ``chain'' would \insist{not} be valid if we were reasoning about equality, because $P \neq (P \disj Q)$.

    Finally, note that \insist{equivalence implies implication}: if we have $P = Q$, we must also have $P \le Q$. We can demonstrate this with a truth table:

    \begin{center}
      \begin{tabular}{c | c ? c ? c ? c}
        $P$ & $Q$ & $P \equ Q$ & $P \impl Q$ & $(P \equ Q) \impl (P \impl Q)$ \\
        \thickhline
        \true & \true & \true & \true & \true \\
        \hline
        \true & \false & \false & \false & \true \\
        \hline
        \false & \true & \false & \true & \true \\
        \hline
        \false & \false & \true & \true & \true
      \end{tabular}
    \end{center}

    Of course, this is not always valid in reverse: implication is a ``weaker'' claim than equivalence.


\section{Boolean algebra}

  With our concepts of \vocab{value-preserving} transformations and transitive reasoning, we are ready to define the algebraic structure of Boolean logic.

  To be precise, there are actually many equivalent ways to define Boolean logic algebraically. The requirements we will list here form a common presentation of the laws of Boolean algebra, but not the only possible presentation.

  Together, these laws can be taken as a \insist{complete definition} of Boolean logic:

  \begin{center}
    \begin{tabular}{c | r c l}
      name & \multicolumn{3}{c}{rule} \\
      \thickhline
      associativity & $P \conj (Q \conj R)$ & = & $(P \conj Q) \conj R$ \\
      \hline
      associativity & $P \disj (Q \disj R)$ & = & $(P \disj Q) \disj R$ \\
      \hline
      commutativity & $P \conj Q$ & = & $Q \conj P$ \\
      \hline
      commutativity & $P \disj Q$ & = & $Q \disj P$ \\
      \hline
      absorption & $P \disj (P \conj Q)$ & = & $P$ \\
      \hline
      absorption & $P \conj (P \disj Q)$ & = & $P$ \\
      \hline
      unit & $P \disj \false$ & = & $P$ \\
      \hline
      unit & $P \conj \true$ & = & $P$ \\
      \hline
      distributivity & $P \conj (Q \disj R)$ & = & $(P \conj Q) \disj (P \conj R)$ \\
      \hline
      distributivity & $P \disj (Q \conj R)$ & = & $(P \disj Q) \conj (P \disj R)$ \\
      \hline
      complement & $P \conj \neg P$ & = & $\false$ \\
      \hline
      complement & $P \disj \neg P$ & = & $\true$ \\
      \hline
      definition of implication & $P \impl Q$ & = & $\neg P \disj Q$ \\
      \hline
      definition of equivalence & $P \equ Q$ & = & $(P \impl Q) \conj (Q \impl P)$
    \end{tabular}
  \end{center}

  What do we mean when we say these laws are a \insist{complete definition} of Boolean logic?

  \begin{itemize}
    \item If we can prove $e = \true$ by equational reasoning with these laws for some Boolean expression $e$, then $e$ is a tautology in Boolean logic.
    \item If we can prove $e = \false$ by equational reasoning with these laws for some Boolean expression $e$, then $e$ is a contradiction in Boolean logic.
    \item If some expression $e$ is a tautology in Boolean logic, then we can prove $e = \true$ by equational reasoning with these laws.
    \item If some expression $e$ is a contradiction in Boolean logic, then we can prove $e = \false$ by equational reasoning with these laws.
  \end{itemize}

  In other words, our truth-table-based definition of Boolean logic and our algebra-based definition of Boolean logic always ``agree'' on the truth value of an expression. This is why we say they are two different ways to define the \insist{same} system of logic, not two \insist{different} systems of logic.

  We will explore examples of these principles in lecture!


\input{include/footer.tex}
\end{document}
