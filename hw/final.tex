\input{include/header.tex}
\input{include/theories/bool.tex}
\input{include/theories/ht.tex}
\input{include/theories/iplc.tex}

\usepackage{wasysym} % APL symbols
\usepackage{anyfontsize} % sizing APL symbols correctly

\title{Final exam}
\subtitle{Material from weeks 1-9}

\begin{document}

\maketitle

\section*{Grading}

  Each exercise in this exam is worth three points, for a total of 30 points.

\section{Boolean truth tables}

  Consider the logical operators defined by these truth tables.

  \begin{center}
    \DISJ \quad \EQU \quad \NCONJ \quad \NDISJ
  \end{center}

  For exercise 1, calculate the value of the expression in the form of a Boolean truth table.

  \begin{enumerate}[noitemsep]
    \item $C \disj ((A \nconj B) \ndisj (B \equ C))$
  \end{enumerate}

\section{Calculating with lookup tables}

  Consider these arithmetic operators over the set \{1, 10, 100\} defined by lookup tables.

  \begin{center}
    \begin{tabular}{c c ? c | c | c}
      \multicolumn{2}{c}{} & \multicolumn{3}{c}{$y$} \\
      & $x\ \APLcomment\ y$ & 1 & 10 & 100 \\
      \thickcline{2-5}
      \multirow{3}{*}{$x$}
      & 1 & 1 & 1 & 1 \\
      \cline{2-5}
      & 10 & 1 & 10 & 100 \\
      \cline{2-5}
      & 100 & 1 & 10 & 100 \\
    \end{tabular}
    \quad
    \begin{tabular}{c c ? c | c | c}
      \multicolumn{2}{c}{} & \multicolumn{3}{c}{$y$} \\
      & $x\ \APLlog\ y$ & 1 & 10 & 100 \\
      \thickcline{2-5}
      \multirow{3}{*}{$x$}
      & 1 & 10 & 100 & 1 \\
      \cline{2-5}
      & 10 & 10 & 1 & 100 \\
      \cline{2-5}
      & 100 & 1 & 10 & 100 \\
    \end{tabular}
    \quad
    \begin{tabular}{c ? c}
      $x$ & $\quarternote\ x$ \\
      \thickhline
      1 & 10 \\
      \hline
      10 & 10 \\
      \hline
      100 & 100
    \end{tabular}
  \end{center}

  For exercises 2 and 3, calculate the value of each provided expression in the form of a lookup table for all possible inputs in the set \{1, 10, 100\}.

  \begin{enumerate}[noitemsep,resume]
    \item $A\ \APLlog\ (\quarternote\ (\quarternote\ A))$
    \item $(A\ \APLlog\ B)\ \APLcomment\ (B\ \APLlog\ A)$
  \end{enumerate}

  We will use the following notation for bitwise operators, based on the Boolean logical operators defined in section 1:

  \begin{center}
    \begin{tabular}{c c}
      logical operator & bitwise operator \\
      \thickhline
      $\neg$ & \texttt{\textasciitilde} \\
      \hline
      $\ndisj$ & \texttt{@} \\
      \hline
      $\nconj$ & \texttt{\#}
    \end{tabular}
  \end{center}

  For exercise 4, calculate the result of the bitwise expression.

  \begin{enumerate}[noitemsep,resume]
    \item \texttt{(1100 \texttt{@} 1111)} \texttt{|} \texttt{\textasciitilde}(0000 \texttt{\#} 0110)
  \end{enumerate}

  For exercise 5, recall how C++ interprets bit vectors as Boolean truth values: a value with all bits set to 0 is a \texttt{false} value, and any other value is a \texttt{true} value.

  \begin{enumerate}[noitemsep,resume]
    \item Show that the bitwise NAND operator (\texttt{\#}) is not the same as the logical NAND operator ($\nconj$), by giving two bit vectors $x$ and $y$ where the result of $x \texttt{\#} y$ is ``wrong'' according to the Boolean logical definition of the $\nconj$ operator.
  \end{enumerate}

\section{Abstract syntax trees}

  Consider the table below, which defines the standard order of operations for Boolean logic. The operators are listed in order from top to bottom, where the top operator ``happens first'' and the bottom operator ``happens last''.

  \begin{center}
    \begin{tabular}{c c c}
      \begin{tabular}[t]{c}
        logic \\
        \thickhline
        $\conj$ \\
        $\disj$ \\
        $\impl$
      \end{tabular}
    \end{tabular}
    \quad
    \begin{tabular}{c c c}
      \begin{tabular}[t]{c}
        made up for this exam \\
        \thickhline
        $\RHD$ \\
        $\LHD$ \\
        $\CIRCLE$ \\
        $\RIGHTCIRCLE$
      \end{tabular}
    \end{tabular}
  \end{center}

  For exercises 6 and 7, draw the abstract syntax tree that corresponds to the expression.

  \begin{enumerate}[noitemsep,resume]
    \item $(A \conj B) \conj C \disj D \conj E \impl F \disj G$
    \item $A \LHD B \RHD C \CIRCLE (D \RIGHTCIRCLE E \RHD F) \RIGHTCIRCLE G$
  \end{enumerate}

\section{Natural deduction}

  Here are the rules that define our system of natural deduction for Boolean logic:

  \begin{center}
    \Variable

    \AndIntro \quad \AndElim

    \OrIntroL \quad \OrIntroR

    \OrElim

    \ImplIntro \quad \ImplElim

    \TopIntro \quad \BotElim
    
    \NotIntro \quad \NotElim

    \LEM
  \end{center}

  For exercise 8, prove the expression by constructing a proof tree using only the natural deduction rules listed above.

  \begin{enumerate}[noitemsep,resume]
    \item $(A \impl C) \conj (B \impl C) \impl (A \disj B \impl C)$
  \end{enumerate}

  For exercise 9, disprove the expression by constructing a proof tree using only the natural deduction rules listed above.

  \begin{enumerate}[noitemsep,resume]
    \item $A \conj \neg (A \disj B)$
  \end{enumerate}

\section{Type theory}

  Here are some rules that define a type theory for a small programming language:

  \begin{center}
    \AxiomC{$\Gamma(x) = t$}
    \LeftLabel{\rulename{Var}}
    \UnaryInfC{$\Gamma \vdash x\ :\ t$}
    \DisplayProof

    \AxiomC{}
    \LeftLabel{\rulename{Bool-True}}
    \UnaryInfC{$\Gamma \vdash \mathtt{true}\ :\ \mathtt{bool}$}
    \DisplayProof
    \quad
    \AxiomC{}
    \LeftLabel{\rulename{Bool-False}}
    \UnaryInfC{$\Gamma \vdash \mathtt{false}\ :\ \mathtt{bool}$}
    \DisplayProof

    \AxiomC{$\Gamma \vdash f\ :\ \texttt{(}t_1\texttt{,}t_2\texttt{)} \to t_3$}
    \AxiomC{$\Gamma \vdash e_1\ :\ t_1$}
    \AxiomC{$\Gamma \vdash e_2\ :\ t_2$}
    \LeftLabel{\rulename{App}$_2$}
    \TrinaryInfC{$\Gamma \vdash f\texttt{(}e_1\texttt{,}e_2\texttt{)}\ :\ t_3$}
    \DisplayProof
  \end{center}

  For exercise 10, prove the statement by constructing a proof tree using the typing rules listed above.

  \begin{enumerate}[noitemsep,resume]
    \item $(f : \texttt{(bool}, \texttt{int)} \to \texttt{int}), (x : \texttt{int}) \vdash (f\texttt{(true}, f\texttt{(false}, x\texttt{)}) : \texttt{int}$
  \end{enumerate}


\end{document}
